// Copyright Epic Games, Inc. All Rights Reserved.

#include "CoordinatesTransformerBPLibrary.h"
#include "CoordinatesTransformer.h"

UCoordinatesTransformerBPLibrary::UCoordinatesTransformerBPLibrary(const FObjectInitializer &ObjectInitializer)
	: Super(ObjectInitializer)
{
}

FMatrix UCoordinatesTransformerBPLibrary::MakeBasis(const FVector &origin, const FVector &x, const FVector &y)
{
	FVector xAxis = (x - origin);
	FVector yAxis = (y - origin);

	xAxis.Normalize();
	yAxis.Normalize();

	FVector zAxis = FVector::CrossProduct(xAxis, yAxis);

	float angleXY = FMath::RadiansToDegrees(FMath::Acos(FVector::DotProduct(xAxis, yAxis)));

	FVector orthogonalYAxis = yAxis.RotateAngleAxis(90 - angleXY, zAxis);
	
	UE_LOG(LogTemp, Warning, TEXT("X Axis %s"), *xAxis.ToString());
	UE_LOG(LogTemp, Warning, TEXT("Y Axis %s"), *yAxis.ToString());
	UE_LOG(LogTemp, Warning, TEXT("Z Axis %s"), *zAxis.ToString());
	UE_LOG(LogTemp, Warning, TEXT("Angle XY %s"), *FString::SanitizeFloat(angleXY));
	UE_LOG(LogTemp, Warning, TEXT("Orthogonal Y Axis %s"), *orthogonalYAxis.ToString());

	FMatrix basis = FMatrix::Identity;

	basis.SetAxis(0, xAxis);
	basis.SetAxis(1, orthogonalYAxis);
	basis.SetAxis(2, zAxis);

	return basis;
}

FMatrix UCoordinatesTransformerBPLibrary::GetTransitionMatrix(const FMatrix &oldBasis, const FMatrix &newBasis)
{
	Chaos::PMatrix<float, 3, 3> byDecompose3 = Chaos::PMatrix<float, 3, 3>(oldBasis);
	Chaos::PMatrix<float, 3, 3> toDecompose3 = Chaos::PMatrix<float, 3, 3>(newBasis);
	Chaos::PMatrix<float, 3, 3> transitionMatrix = Chaos::PMatrix<float, 3, 3>();
	transitionMatrix.M[3][3] = 1;

	for (int i = 0; i < 3; i++)
	{
		transitionMatrix.SetColumn(
			i,
			toDecompose3.GetRow(i).X * byDecompose3.GetRow(0) +
				toDecompose3.GetRow(i).Y * byDecompose3.GetRow(1) +
				toDecompose3.GetRow(i).Z * byDecompose3.GetRow(2));
	}

	return transitionMatrix;
}

FMatrix UCoordinatesTransformerBPLibrary::OrthonormalizeBasis(const FMatrix &basis)
{
	Chaos::PMatrix<float, 3, 3> basis3 = Chaos::PMatrix<float, 3, 3>(basis);
	FVector basisVectors[3];

	for (short i = 0; i < 3; i++)
	{
		basisVectors[i] = basis3.GetRow(i);
	}

	FVector::CreateOrthonormalBasis(
		basisVectors[0],
		basisVectors[1],
		basisVectors[2]);

	basis3.SetAxes(
		&basisVectors[0],
		&basisVectors[1],
		&basisVectors[2]);

	return basis3;
}

FVector UCoordinatesTransformerBPLibrary::CalculateCoordinates(const FMatrix &transitionMatrix, const FVector &newOriginInOld, const FVector &coordinates)
{
	Chaos::PMatrix<float, 3, 3> transitionMatrix3 = Chaos::PMatrix<float, 3, 3>(transitionMatrix);
	FVector oldOriginInNew = -(transitionMatrix3 * newOriginInOld);
	FVector newCoordinates = transitionMatrix3 * coordinates + oldOriginInNew;

	return newCoordinates;
}

FVector UCoordinatesTransformerBPLibrary::TransformCoordinates(const FMatrix &oldBasis, const FMatrix &newBasis, const FVector &newOriginInOld, const FVector &coordinates)
{
	FMatrix transitionMatrix = GetTransitionMatrix(oldBasis, newBasis);
	FMatrix inversedTransitionMatrix = transitionMatrix.Inverse();

	return CalculateCoordinates(inversedTransitionMatrix, newOriginInOld, coordinates);
}
