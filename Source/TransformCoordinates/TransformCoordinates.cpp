// Copyright Epic Games, Inc. All Rights Reserved.

#include "TransformCoordinates.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TransformCoordinates, "TransformCoordinates" );
